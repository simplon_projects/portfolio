# Port Folio Simplon

## Sophie Brunier

Ceci est mon projet port-folio de la Fabrique Simplon Lyon, vous y trouverez des infos sur mon parcours, les langages appris en formation, et même mes différents projets Git.



### Pourquoi un port-Folio ? (UserStories)

1. En tant que *Simplonnienne*, je veux un port-folio accessible sur le net pour présenter ce que je sais faire à d'éventuels futurs recruteurs.
2. En tant qu'éventuel *recruteur*, je veux pouvoir visualiser le travail d'un candidat pour évaluer son niveau.
3. En tant que *formateur*, je veux pourvoir visualiser l'avancé du port-folio des apprenants pour voir leur évolution.
4. En tant que *Simplonnienne*, je veux voir les port-folio d'autres apprenants pour m'inspirer, et voir ce qu'il est possible de faire.



### Maquette fonctionnelle :

![](./MaquettePortFolio.jpg)



### Méthodes utilisées:

- Lybrary Bootstrap pour l'adaptabilité
- Icones Fontawesome
- Fichier CSS poyur le style
- JavaScript pour animation de titre

